﻿using DemoStorage.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO;
using System.Web.Mvc;

namespace DemoStorage.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Upload()
        {
            var viewModel = new UploadViewModel();
            return View();
        }

        [HttpPost]
        public ActionResult Upload(UploadViewModel file)
        {
            if (file.FileUpload != null)
            {
                // Create the blob client.
                CloudBlobClient blobClient = StorageAccount.CreateCloudBlobClient();

                // Get container named "images", create new if not exist
                CloudBlobContainer container = blobClient.GetContainerReference("images");
                container.CreateIfNotExists();

                //Set permission
                BlobContainerPermissions permission = new BlobContainerPermissions();
                permission.PublicAccess = BlobContainerPublicAccessType.Container;
                container.SetPermissions(permission);

                // Retrieve reference to a blob named "image".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(file.FileUpload.FileName);

                using (var fs = file.FileUpload.InputStream)
                {
                    fs.Seek(0, SeekOrigin.Begin);
                    // Create or overwrite the "image" blob with contents from a local file.
                    blockBlob.UploadFromStream(fs);
                }
                ViewBag.Message = "File successfully uploaded!";
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["DemoStorageConnectionString"].ConnectionString;
            }
        }

        public CloudStorageAccount StorageAccount
        {
            get
            {
                return CloudStorageAccount.Parse(ConnectionString);
            }
        }
    }
}