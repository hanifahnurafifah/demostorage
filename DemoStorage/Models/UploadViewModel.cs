﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoStorage.Models
{
    public class UploadViewModel
    {
        [Display(Name = "Upload your file")]
        public HttpPostedFileBase FileUpload { get; set; }
    }
}